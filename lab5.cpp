/*
Sean Craffey
scraffe
Lab 5
006
Anurata Prabha Hridi
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
#include <ctime>

//Enum for the suit.
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Card struct.
typedef struct Card {
  Suit suit;
  int value;
} Card;

//Function Prototypes.
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  int i = 0;
  int num = 2;
  
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  //Creating a Deck and initializing the card value and suit.
  Card deck[52];

  for(i = 0; i < 52; i++)
  {
    deck[i].value = num;
    num++;
    if(num > 14)
    {
      num = 2;
    }
  }

  num = 0;
  for(i = 0; i < 52; i++)
  {
    deck[i].suit = static_cast<Suit>(num);
    if(i == 13 || i == 26 || i == 39)
    {
      num++;
    }  
  }


  //Shuffling the Deck.
  random_shuffle(&deck[0] , &deck[52], &myrandom);



  //Creating the hand from the first 5 cards from the deck.
  Card hand[5];
  for(i = 0; i < 5; i++)
  {
    hand[i].suit = deck[i].suit;
    hand[i].value = deck[i].value;
  }

  //Sorting the Hand.
  sort(&hand[0], &hand[5], suit_order);
 
  /* Card temp;
  for(i = 0; i < 4; i++)
  {
    if(suit_order(hand[i], hand[i+1]) != true) 
    {
       hand[i].suit = temp.suit;
       hand[i].value = temp.value;
       hand[i].suit = hand[i+1].suit;
       hand[i].value = hand[i+1].value;
       hand[i+1].suit = temp.suit;
       hand[i+1].value = temp.value;
    }
   }
  */
   
   //Displaying the card names and suits on the users screen.
   for(i = 0; i < 5; i++)
   {
      cout << "\t" << get_card_name(hand[i]) << " " << get_suit_code(hand[i]) << "\n";
   }


  return 0;
}


//Ordering the suits of the cards.
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit) {return true;}
  else if(lhs.suit == rhs.suit && lhs.value < rhs.value) {return true;}
  
  return false; 
}

//Getting the code for the suit symbol.
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Getting the name of the card to be outputed to the users screen.
string get_card_name(Card& c) 
{
  if(c.value == 11)
  {
    return "Jack of";
  }
  else if(c.value == 12)
  {
    return "Queen of";
  }
  else if(c.value == 13)
  {
    return "King of";
  }
  else if(c.value == 14)
  {
    return "Ace of"; 
  }
  else
  {
    return to_string(c.value) + " of";
  }
}
